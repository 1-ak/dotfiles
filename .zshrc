# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# zsh history
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history


# zsh auto/tab completion
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots) # include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1


# startx configuration
[[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx --vt1


# system aliases
alias vim="nvim"
alias ll="ls -l"
alias la="ls -a"
alias l="ls -al"
alias pS="sudo pacman -S"
export PATH="$HOME/.emacs.d/bin:$PATH"


# git dotfiles aliases
alias dotdir="/usr/bin/git --git-dir=$HOME/GitRepositories/dotfiles --work-tree=$HOME"
alias dotcommit='DATE=$(date) && dotdir commit -m "Local Update ${DATE}"'
alias dotadd="dotdir add"
alias dotpush="dotdir push"


# scripts aliases
alias backlight="$HOME/GitRepositories/bashscripts/xrandr/xbacklight.sh"

# my website
alias zshrc="vim $HOME/.zshrc"
alias cdhomepage="cd /home/aman/GitRepositories/amankushwaha.in/homepage/"
alias g="git"











# zsh plugins 
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
